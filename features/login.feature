# language: pl
Potrzeba biznesowa: Logowanie na banbye.pl

  Szablon scenariusza: Będąc użytkownikiem, mogę się zalogować na BanBye

  #Zakładając, że jestem na stronie banbye
  #Gdy loguję się używając nazwy użytkownika <email> i hasła <password>
  #Wtedy powinienem zobaczyć ...

  @desktop
  Przykłady:
    | email                     | password |
    | banbyetest@protonmail.com | 1234!@#$ |
