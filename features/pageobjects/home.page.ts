import { ChainablePromiseElement, ChainablePromiseArray, ElementArray } from 'webdriverio';

import Page from './page';

/**
 * Podstrona zawierająca selektory i metody używane przez test na stronie internetowej.
 */
class HomePage extends Page {
  /**
   * Definiuj selektory używając metody typu get.
   */
  public get pageTitle(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
    return $('head > title');
  }

  public get aboutCookies(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
    return $('#app > div:nth-child(5) > div > div > div > div:nth-child(1) > div');
  }

  public get Buttons(): ChainablePromiseArray<ElementArray> {
    return $$('button[type="button"]');
  }

  public get settingsButton(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
    return $('.v-btn__content=Ustawienia');
  }

  public get acceptButton(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
    return $('.v-btn__content=Akceptuję');
  }

  public get cookiesSettings(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
    return $('.v-card__title=Ustawienia Cookies');
  }

  public get cookiesCheckbox(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
    return $('#input-1243');
  }

  public get cookiesCheckboxName(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
    return $('.v-input__slot=Wymagane Cookies');
  }

  public get aboutRequiredCookies(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
    return $('#app > div:nth-child(7) > div > div > div.v-card__text > p:nth-child(2)');
  }

  public get matomoCheckbox(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
    return $('#input-1247');
  }

  public get matomoCheckboxName(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
    return $('.v-input__control=Matomo');
  }

  public get matomoServiceMessage(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
    return $('#app > div:nth-child(7) > div > div > div.v-card__text > p:nth-child(4)');
  }

  public get saveAndCloseButton(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
    return $('.v-btn__content=Zapisz i zamknij');
  }

  public get noThankYouButton(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
    return $('.v-btn__content=Nie, dziękuję');
  }

  public get selectLanguageButton(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
    return $('.v-btn__content=English');
  }

  public get selectThemeDark(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
    return $('#app > div.v-dialog__content.v-dialog__content--active > div > div > div.row.no-gutters > div:nth-child(2) > button');
  }

  public get whatWillYouLose(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
    return $('#nothanks > div > h1');
  }

  public get aboutLostAdvantages(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
    return $('#nothanks > div > p');
  }

  public get continueWithoutLoggingIn(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
    return $('.v-btn__content=Przeglądam bez konta');
  }

  public get goBack(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
    return $('.v-btn__content=Wróć');
  }

  public open(): Promise<string> {
    return super.open('HomePage');
  }
}

export default new HomePage();
