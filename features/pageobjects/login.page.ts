import { ChainablePromiseElement } from 'webdriverio';

import Page from './page';

/**
 * Podstrona zawierająca selektory i metody używane przez test na stronie internetowej.
 */
class LoginPage extends Page {
  /**
   * Definiuj selektory używając metody typu get.
   */
  public get pageTitle(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
    return $('head > title');
  }

  public open(): Promise<string> {
    return super.open('LoginPage');
  }
}

export default new LoginPage();
