# language: pl
Potrzeba biznesowa: Strona domowa BanBye używa ciasteczek

  Szablon scenariusza: Będąc użytkownikiem korzystam z ciasteczek

    Zakładając, że jestem na stronie głównej BanBye
    Gdy w nagłówku strony wyświetla się tytuł: <title>
    Wtedy powinienem zobaczyć następującą informację o ciasteczkach: <cookiesMessage>
    Jeżeli na stronie domowej BanBye jest dostępnych <buttons> przycisków
    Wtedy dostępny jest przycisk <cookiesSettingsButton>
    Gdy klikam przycisk <cookiesSettingsButton>
    Wtedy wyświetlają się <cookiesSettings>
    Oraz tekst tytułowy: <cookiesSettings>
    I nazwa pierwszego checkbox: <cookiesCheckboxName>
    Oraz informacja o wymaganych ciasteczkach: <aboutRequiredCookies>
    I checkbox <cookiesCheckboxName> jest domyślnie nieaktywny w pozycji wyłączony
    Oraz nazwa drugiego checkbox: <matomoCheckboxName>
    I tekst o serwisie analitycznym: <matomoServiceMessage>
    Oraz przycisk: <saveAndClose>
    I checkbox <matomoCheckboxName> jest domyślnie aktywny
    Gdy klikam checkbox <matomoCheckboxName>
    Wtedy checkbox <matomoCheckboxName> jest nieaktywny
    Gdy zapisuję ustawienia ciasteczek przyciskiem <saveAndClose>
    Wtedy można kliknąć przycisk <acceptButton>
    Gdy akceptuję ciasteczka przyciskiem <acceptButton>
    Wtedy przycisk <noThankYouButton> jest aktywny
    Gdy wybieram przycisk <noThankYouButton>
    Wtedy w nowym oknie dialogowym wyświetla się przycisk wyboru języka: <selectLanguage>
    Oraz przycisk ciemnego trybu wyświetlania
    I pytanie o utratę korzyści: <whatWillYouLose>
    Oraz informacja o utraconych korzyściach: <aboutLostAdvantages>
    I przycisk <continueWithoutLoggingIn> umożliwiający przeglądanie bez konta
    Oraz przycisk <back> umożliwiający powrót do ekranu logowania
    Gdy korzystam z przycisku <continueWithoutLoggingIn>

    @desktop
    Przykłady:
      | title                                | buttons | cookiesMessage                                                                                                                                                                                                                | cookiesSettingsButton | acceptButton | cookiesSettings   | cookiesCheckboxName | aboutRequiredCookies                                                                                                                                                                                                                   | matomoCheckboxName | matomoServiceMessage                      | saveAndClose   | noThankYouButton | selectLanguage | whatWillYouLose | aboutLostAdvantages                                                                                                                                                | continueWithoutLoggingIn | back |
      | BanBye - Prawda zasługuje na wolność | 11      | BanBye.pl używa cookies oraz technologii umożliwiających anonimowe badanie zachowań użytkowników celem usprawnienia działania strony. Przeglądając BanBye.pl wyrażasz zgodę na stosowanie cookies oraz narzędzi analitycznych. | USTAWIENIA              | AKCEPTUJĘ     | Ustawienia Cookies | Wymagane Cookies      | Te cookies są wymagane do poprawnego działania strony i nie mogą zostać wyłączone.Nie zbierają ani nie wysyłają one żadnych danych o użytkowniku, a służą umożliwieniu poprawnego działania funkcji takich jak np. logowanie w serwisie. | Matomo          | Matomo jest serwisem służącym do analityki. | ZAPISZ I ZAMKNIJ | NIE, DZIĘKUJĘ       | ENGLISH         | Co tracisz?        | Konto na BanBye pozwala Ci komentować i oceniać filmy oraz komentarze pod filmami, a także daje Ci dostęp do powiadomień o nowych filmach z subskrybowanych kanałów. | PRZEGLĄDAM BEZ KONTA        | WRÓĆ |
