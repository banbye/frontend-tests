# language: pl
Potrzeba biznesowa: Rejestracja na banbye.pl

  Szablon scenariusza: Będąc użytkownikiem, mogę się zarejestrować na BanBye

  #Zakładając, że jestem na stronie banbye
  #Gdy rozpoczynam rejestrację klikając przycisk <register>
  #Wtedy powinienem zobaczyć ...

  @desktop
  Przykłady:
    | register        |
    | ZAREJESTRUJ SIĘ |
