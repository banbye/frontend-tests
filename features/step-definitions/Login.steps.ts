import { Given, When, Then } from '@wdio/cucumber-framework';

import LoginPage from '../pageobjects/login.page';
import HomePage from '../pageobjects/home.page';

const pages = {
  BanBye: LoginPage,
};

Given(/^jestem na stronie (\w+)$/, async (page) => {
  await pages[page].open();
});

When(/^loguję się używając nazwy użytkownika (\w+) i hasła (.+)$/, async (username, password) => {
//
});

Then(/^powinienem zobaczyć ... $/, async () => {
//
});
