import { Given, When, Then } from '@wdio/cucumber-framework';

import RegisterPage from '../pageobjects/register.page';
import HomePage from '../pageobjects/home.page';

const pages = {
  BanBye: RegisterPage,
};

Given(/^jestem na stronie (\w+)$/, async (page) => {
  await pages[page].open();
});

When(/^rozpoczynam rejestrację klikając przycisk (.*)$/, async (register) => {
//
});

Then(/^powinienem zobaczyć ... (.*)$/, async () => {
//
});
