import { Given, When, Then } from '@wdio/cucumber-framework';
import HomePage from '../pageobjects/home.page';

const pages = {
  BanBye: HomePage,
};

Given(/^jestem na stronie głównej (\w+)$/, async (page) => {
  await pages[page].open();
});

When(/^w nagłówku strony wyświetla się tytuł: (.*)$/, async (title) => {
  await expect(HomePage.pageTitle).toHaveText(title);
});

Then(/^powinienem zobaczyć następującą informację o ciasteczkach: (.*)$/, async (cookiesMessage) => {
  expect(HomePage.aboutCookies.isDisplayedInViewport());
  await expect(HomePage.aboutCookies).toHaveText(cookiesMessage);
});

When(/^na stronie domowej BanBye jest dostępnych (\d+) przycisków$/, async (buttons) => {
  await expect(HomePage.Buttons).toBeElementsArrayOfSize(buttons);
});

Then(/^dostępny jest przycisk (.*)$/, async (cookiesSettingsButton) => {
  expect(HomePage.settingsButton.isClickable());
  await expect(HomePage.settingsButton).toHaveText(cookiesSettingsButton);
});

When(/^klikam przycisk (.*)$/, async (cookiesSettingsButton) => {
  await (HomePage.settingsButton).click();
});

Then(/^wyświetlają się (.*)$/, async (cookiesSettings) => {
  await expect(HomePage.cookiesSettings).toHaveText(cookiesSettings);
});

Then(/^tekst tytułowy: (.*)$/, async (cookiesSettings) => {
  await expect(HomePage.cookiesSettings).toHaveText(cookiesSettings);
});

Then(/^nazwa pierwszego checkbox: (.*)$/, async (cookiesCheckboxName) => {
  await expect(HomePage.cookiesCheckboxName).toHaveText(cookiesCheckboxName);
});

Then(/^informacja o wymaganych ciasteczkach: (.*)$/, async (aboutRequiredCookies) => {
  await expect(HomePage.aboutRequiredCookies).toHaveText(aboutRequiredCookies);
});

Then(/^checkbox (.*) jest domyślnie nieaktywny w pozycji wyłączony$/, async (cookiesCheckboxName) => {
  expect(HomePage.cookiesCheckbox.isDisplayedInViewport());
  await expect(HomePage.cookiesCheckbox).toHaveAttributeContaining('aria-disabled', 'true');
  await expect(HomePage.cookiesCheckbox).toHaveAttributeContaining('aria-checked', 'false');
});

Then(/^nazwa drugiego checkbox: (.*)$/, async (matomoCheckboxName) => {
  await expect(HomePage.matomoCheckboxName).toHaveText(matomoCheckboxName);
});

Then(/^tekst o serwisie analitycznym: (.*)$/, async (matomoServiceMessage) => {
  await expect(HomePage.matomoServiceMessage).toHaveText(matomoServiceMessage);
});

Then(/^przycisk: (.*)$/, async (saveAndClose) => {
  expect(HomePage.saveAndCloseButton.isDisplayedInViewport());
  await expect(HomePage.saveAndCloseButton).toHaveText(saveAndClose);
});

Then(/^checkbox (.*) jest domyślnie aktywny$/, async (matomoCheckboxName) => {
  expect(HomePage.matomoCheckbox.isDisplayedInViewport());
  await expect(HomePage.matomoCheckbox).toHaveAttributeContaining('aria-disabled', 'false');
});

When(/^klikam checkbox (.*)$/, async (matomoCheckboxName) => {
  await HomePage.matomoCheckbox.click();
});

Then(/^checkbox (.*) jest nieaktywny$/, async (matomoCheckboxName) => {
  await expect(HomePage.matomoCheckbox).toHaveAttributeContaining('aria-checked', 'false');
});

When(/^zapisuję ustawienia ciasteczek przyciskiem (.*)$/, async (saveAndClose) => {
  await HomePage.saveAndCloseButton.click();
});

Then(/^można kliknąć przycisk (.*)$/, async (acceptButton) => {
  await expect(HomePage.acceptButton).toBeClickable();
  await expect(HomePage.acceptButton).toHaveText(acceptButton);
});

When(/^akceptuję ciasteczka przyciskiem (.*)$/, async (acceptButton) => {
  await HomePage.acceptButton.click();
});

Then(/^przycisk (.*) jest aktywny$/, async (noThankYouButton) => {
  await expect(HomePage.noThankYouButton).toBeClickable();
  await expect(HomePage.noThankYouButton).toHaveText(noThankYouButton);
});

When(/^wybieram przycisk (.*)$/, async (noThankYouButton) => {
  await HomePage.noThankYouButton.click();
});

Then(/^w nowym oknie dialogowym wyświetla się przycisk wyboru języka: (.*)$/, async (selectLanguage) => {
  await expect(HomePage.selectLanguageButton).toBeClickable();
});

Then(/^przycisk ciemnego trybu wyświetlania$/, async () => {
  await expect(HomePage.selectThemeDark).toBeClickable();
});

Then(/^pytanie o utratę korzyści: (.*)$/, async (whatWillYouLose) => {
  expect(HomePage.whatWillYouLose.isDisplayedInViewport());
  await expect(HomePage.whatWillYouLose).toHaveText(whatWillYouLose);
});

Then(/^informacja o utraconych korzyściach: (.*)$/, async (aboutLostAdvantages) => {
  expect(HomePage.aboutLostAdvantages.isDisplayedInViewport());
  await expect(HomePage.aboutLostAdvantages).toHaveText(aboutLostAdvantages);
});

Then(/^przycisk (.*) umożliwiający przeglądanie bez konta$/, async (continueWithoutLoggingIn) => {
  await expect(HomePage.continueWithoutLoggingIn).toBeClickable();
  await expect(HomePage.continueWithoutLoggingIn).toHaveText(continueWithoutLoggingIn);
});

Then(/^przycisk (.*) umożliwiający powrót do ekranu logowania$/, async (back) => {
  await expect(HomePage.goBack).toBeClickable();
  await expect(HomePage.goBack).toHaveText(back);
});

When(/^korzystam z przycisku (.*)$/, async (continueWithoutLoggingIn) => {
  await HomePage.continueWithoutLoggingIn.click();
});
