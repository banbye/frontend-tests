
# BanBye - testy frontendu

#### Opis
Testy frontendu BanBye zostały zaimplementowane z wykorzystaniem framework WebdriverIO,
ponieważ wspiera on natywnie Vue i mają służyć sprawdzaniu regresji.
Teoretycznie można je wykorzystać także dla TDD, np. podczas pisania aplikacji mobilnych,
testowania aplikacji na wielu urządzeniach itp.
Pierwszy commit to boilerplate dla BanBye, a potem nastąpi dalszy rozwój tego oprogramowania.

#### Instalacja

1. git clone https://gitlab.com/banbye/frontend-tests.git
2. cd frontend-tests
3. git checkout main
4. npm i

#### Uruchomienie

1.  npm run wdio

#### Kontrybucja

1.  Wykonanie forka repozytorium
2.  Utworzenie gałęzi z nową funkcjonalnością
3.  Zakomitowanie swojego kodu
4.  Utworzenie Pull Request

#### Wskazówka
Ponieważ frontend testowanej aplikacji pochodzący z frameworka jest charakterystyczny,
podczas definiowania selektorów najlepiej korzystać z nazw klas oraz z atrybutów klas,
a nie z identyfikatorów. Stosowane identyfikatory są generowane automatycznie i się zmieniają.
Korzystnie jest mieć na uwadze, że część metod WebdriverIO jest w takim układzie nieprzydatna.
Dlatego przed ich użyciem dobrze jest się wczytać w to, co wybrana metoda robi.

#### Możliwe udogodnienia

Użytkownik końcowy widzi wycinek całości, natomiast framework WebdriverIO przewiduje
funkcjonalności, które potrafią całościowo objąć temat regresji, lecz nie zostały zaimplementowane,
m.in. z następujących powodów:

1. Nie każdy używa testów w Pipeline, chociaż to błąd.
2. Nie każdy używa Dockerfile.
3. Nie każdy chce korzystać z serwisów w chmurze testujących aplikację na wielu urządzeniach.
4. Raportowanie wyników i tak trzeba dostosować do własnych potrzeb.
5. Reszta w dokumentacji na stronie https://webdriver.io/ .
